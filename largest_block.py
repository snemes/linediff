#!/usr/bin/env python
import sys
import os

__author__ = 'snemes'

def largest_block(s1, s2, minsize=16):

    l1 = len(s1)
    l2 = len(s2)

    if l1 < l2:
        s1, s2 = s2, s1
        l1, l2 = l2, l1

    for i in range(0, l1-minsize+1):
        for j in range(l1, minsize+i-1, -1):
            k = s2.find(s1[i:j])
            if k < 0: continue
            l = k + j - i
            assert s1[i:j] == s2[k:l]
            return i, j, s1[i:j].encode("hex")

    return None

def main():
    if len(sys.argv) < 3:
        print("USAGE: {0} <file1> <file2>".format(os.path.basename(sys.argv[0])))
        sys.exit(0)

    data1 = None
    with open(sys.argv[1], "rb") as f:
        data1 = f.read()

    data2 = None
    with open(sys.argv[2], "rb") as f:
        data2 = f.read()

    print("{0:08x} {1:08x} {2}".format(*largest_block(data1, data2)))

if __name__ == "__main__":
    main()
