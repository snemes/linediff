#!/usr/bin/env python
import sys

__author__ = 'snemes'

HUNK_UNCHANGED = " "
HUNK_CHANGED = "!{}"
HUNK_ADDED = "+"
HUNK_DELETED = "-"

# Longest Common Subsequence - iterative Hunt-McIlroy algorithm
def lcs(s, t):
    m, n = len(s), len(t)
    P = [[None for i in range(n+1)] for i in range(m+1)]

    for i in range(m+1):
        P[i][0] = []

    for j in range(n+1):
        P[0][j] = []

    for i in range(1, m+1):
        for j in range(1, n+1):
            if s[i-1] == t[j-1]:
                P[i][j] = P[i-1][j-1] + [(i-1,j-1)]
            else:
                if len(P[i-1][j]) > len(P[i][j-1]):
                    P[i][j] = P[i-1][j]
                else:
                    P[i][j] = P[i][j-1]

    return P[m][n]


# Naive algorithm for longest common substring (and not subsequence!)
def linediff(s1, s2, minsize=1):

    if s1 and not s2:
        return ((s1, HUNK_DELETED),)
    elif not s1 and s2:
        return ((s2, HUNK_ADDED),)
    elif not s1 and not s2:
        return ()

    # swap strings is first is longer
    l1 = len(s1)
    l2 = len(s2)
    for i in range(0, l1-minsize+1):
        for j in range(l1, minsize+i-1, -1):
            k = s2.find(s1[i:j])
            if k < 0: continue
            l = k + j - i
            assert s1[i:j] == s2[k:l]
            return linediff(s1[:i], s2[:k]) + ((s1[i:j], HUNK_UNCHANGED),) + linediff(s1[j:], s2[l:])

    return ((s2, HUNK_CHANGED.format(s1)),)


def linediff2(s1, s2):

    # collect contiguous blocks
    hunks = [[(0, 0), (0, 0)]]
    for m in lcs(s1, s2):
        if m[0] != hunks[-1][1][0] or m[1] != hunks[-1][1][1] or len(hunks) < 2:
            hunks.append([m, (m[0] + 1, m[1] + 1)])
        else:
            hunks[-1][1] = (m[0] + 1, m[1] + 1)
    hunks.append([(len(s1), len(s2)), (len(s1), len(s2))])

    # fill in added/deleted/changed hunks
    result = []
    for i in range(1, len(hunks)):
        hunk = (s1[hunks[i-1][1][0]:hunks[i][0][0]], s2[hunks[i-1][1][1]:hunks[i][0][1]])
        if hunk[0]:
            if hunk[1]:
                result.append((hunk[1], HUNK_CHANGED.format(hunk[0])))
            else:
                result.append((hunk[0], HUNK_DELETED))
        elif hunk[1]:
                result.append((hunk[1], HUNK_ADDED))
        hunk = (s1[hunks[i][0][0]:hunks[i][1][0]], s2[hunks[i][0][0]:hunks[i][1][0]])
        if len(hunk[0]) > 0:
            result.append((hunk[0], HUNK_UNCHANGED))

    return tuple(result)


def disp1(d):
    result = ""
    for i, j in d:
        result += {
            " ": "{}",
            "+": "<add>{}</add>",
            "-": "<del>{}</del>",
            "!": "<chg>{}</chg>"
        }[j[0]].format(i)
    return result

def disp2(d):
    line1 = ""
    line2 = ""
    for i, j in d:
        line1 += i
        line2 += len(i)*j[0]
    return "{0}\n{1}".format(line1, line2)

def main():
    if len(sys.argv) > 2:
        print disp2(linediff(sys.argv[1], sys.argv[2]))

    # unit tests
    assert linediff ("zabcdz", "xyab_cdyx") == (('xy', '!z'), ('ab', ' '), ('_', '+'), ('cd', ' '), ('yx', '!z'))
    assert linediff2("zabcdz", "xyab_cdyx") == (('xy', '!z'), ('ab', ' '), ('_', '+'), ('cd', ' '), ('yx', '!z'))

    assert linediff ("ZZABCZDEF", "AB_CD_EF_GH_ABCD") == (('AB_CD_EF_GH_', '!ZZ'), ('ABC', ' '), ('Z', '-'), ('D', ' '), ('EF', '-'))
    assert linediff2("ZZABCZDEF", "AB_CD_EF_GH_ABCD") == (('ZZ', '-'), ('AB', ' '), ('_', '+'), ('C', ' '), ('Z', '-'), ('D', ' '), ('_', '+'), ('EF', ' '), ('_GH_ABCD', '+'))

    assert linediff ("ABCDEF", "AB_CD_EFABCD") == (('AB_CD_EF', '+'), ('ABCD', ' '), ('EF', '-'))
    assert linediff2("ABCDEF", "AB_CD_EFABCD") == (('AB', ' '), ('_', '+'), ('CD', ' '), ('_', '+'), ('EF', ' '), ('ABCD', '+'))

    assert linediff ("ABCDEFGHIJKLMNOPQRSTUVWXYZ", "AB_CD_EF_GH_IJ_KL_MN_OP_QR_ABCDEF") == (('AB_CD_EF_GH_IJ_KL_MN_OP_QR_', '+'), ('ABCDEF', ' '), ('GHIJKLMNOPQRSTUVWXYZ', '-'))
    assert linediff2("ABCDEFGHIJKLMNOPQRSTUVWXYZ", "AB_CD_EF_GH_IJ_KL_MN_OP_QR_ABCDEF") == (('AB', ' '), ('_', '+'), ('CD', ' '), ('_', '+'), ('EF', ' '), ('_', '+'), ('GH', ' '), ('_', '+'), ('IJ', ' '), ('_', '+'), ('KL', ' '), ('_', '+'), ('MN', ' '), ('_', '+'), ('OP', ' '), ('_', '+'), ('QR', ' '), ('_ABCDEF', '!STUVWXYZ'))

    assert linediff ("abcdefghijklmnopqrstuvwxyz", "abcdefghijklnmopqrstuvwxyz") == (('abcdefghijkl', ' '), ('n', '+'), ('m', ' '), ('n', '-'), ('opqrstuvwxyz', ' '))
    assert linediff2("abcdefghijklmnopqrstuvwxyz", "abcdefghijklnmopqrstuvwxyz") == (('abcdefghijkl', ' '), ('m', '-'), ('n', ' '), ('m', '+'), ('opqrstuvwxyz', ' '))

    assert linediff ("abcdefghijklmnopqrstuvwxyz", "abcdefgh_ijklmnopq_rstuvwxyz") == (('abcdefgh', ' '), ('_', '+'), ('ijklmnopq', ' '), ('_', '+'), ('rstuvwxyz', ' '))
    assert linediff2("abcdefghijklmnopqrstuvwxyz", "abcdefgh_ijklmnopq_rstuvwxyz") == (('abcdefgh', ' '), ('_', '+'), ('ijklmnopq', ' '), ('_', '+'), ('rstuvwxyz', ' '))

    assert linediff ("abcdefghijklmnopqrstuvwxyz", "abcdefghijkl_nopqrstuvwxyz") == (('abcdefghijkl', ' '), ('_', '!m'), ('nopqrstuvwxyz', ' '))
    assert linediff2("abcdefghijklmnopqrstuvwxyz", "abcdefghijkl_nopqrstuvwxyz") == (('abcdefghijkl', ' '), ('_', '!m'), ('nopqrstuvwxyz', ' '))

    assert linediff ("abcdefghijklmnopqrstuvwxyz", "abcdefghijklmnopqrstuvwxyzAAA") == (('abcdefghijklmnopqrstuvwxyz', ' '), ('AAA', '+'))
    assert linediff2("abcdefghijklmnopqrstuvwxyz", "abcdefghijklmnopqrstuvwxyzAAA") == (('abcdefghijklmnopqrstuvwxyz', ' '), ('AAA', '+'))

    assert linediff ("abcdefghijklmnopqrstuvwxyz", "AAAabcdefghijklmnopqrstuvwxyz") == (('AAA', '+'), ('abcdefghijklmnopqrstuvwxyz', ' '))
    assert linediff2("abcdefghijklmnopqrstuvwxyz", "AAAabcdefghijklmnopqrstuvwxyz") == (('AAA', '+'), ('abcdefghijklmnopqrstuvwxyz', ' '))

    assert linediff ("abcdefghijklmnopqrstuvwxyz", "abcdefghijklAAAmnopqrstuvwxyz") == (('abcdefghijkl', ' '), ('AAA', '+'), ('mnopqrstuvwxyz', ' '))
    assert linediff2("abcdefghijklmnopqrstuvwxyz", "abcdefghijklAAAmnopqrstuvwxyz") == (('abcdefghijkl', ' '), ('AAA', '+'), ('mnopqrstuvwxyz', ' '))

    assert linediff ("abcdefghijklmnopqrstuvwxyz", "abcdefghijklmnopqrstuvw") == (('abcdefghijklmnopqrstuvw', ' '), ('xyz', '-'))
    assert linediff2("abcdefghijklmnopqrstuvwxyz", "abcdefghijklmnopqrstuvw") == (('abcdefghijklmnopqrstuvw', ' '), ('xyz', '-'))

    assert linediff ("abcdefghijklmnopqrstuvwxyz", "defghijklmnopqrstuvwxyz") == (('abc', '-'), ('defghijklmnopqrstuvwxyz', ' '))
    assert linediff2("abcdefghijklmnopqrstuvwxyz", "defghijklmnopqrstuvwxyz") == (('abc', '-'), ('defghijklmnopqrstuvwxyz', ' '))

    assert linediff ("abcdefghijklmnopqrstuvwxyz", "abcdefghijklpqrstuvwxyz") == (('abcdefghijkl', ' '), ('mno', '-'), ('pqrstuvwxyz', ' '))
    assert linediff2("abcdefghijklmnopqrstuvwxyz", "abcdefghijklpqrstuvwxyz") == (('abcdefghijkl', ' '), ('mno', '-'), ('pqrstuvwxyz', ' '))

    assert linediff ("abcdefghijklmnopqrstuvwxyz", "abcdefghijklmnopqrstuvw___") == (('abcdefghijklmnopqrstuvw', ' '), ('___', '!xyz'))
    assert linediff2("abcdefghijklmnopqrstuvwxyz", "abcdefghijklmnopqrstuvw___") == (('abcdefghijklmnopqrstuvw', ' '), ('___', '!xyz'))

    assert linediff ("abcdefghijklmnopqrstuvwxyz", "___defghijklmnopqrstuvwxyz") == (('___', '!abc'), ('defghijklmnopqrstuvwxyz', ' '))
    assert linediff2("abcdefghijklmnopqrstuvwxyz", "___defghijklmnopqrstuvwxyz") == (('___', '!abc'), ('defghijklmnopqrstuvwxyz', ' '))

    assert linediff ("abcdefghijklmnopqrstuvwxyz", "abcdefghijkl___pqrstuvwxyz") == (('abcdefghijkl', ' '), ('___', '!mno'), ('pqrstuvwxyz', ' '))
    assert linediff2("abcdefghijklmnopqrstuvwxyz", "abcdefghijkl___pqrstuvwxyz") == (('abcdefghijkl', ' '), ('___', '!mno'), ('pqrstuvwxyz', ' '))

    assert linediff ("abcdefghijklmnopqrstuvwxyz", "zyxwvutsrqponmlkjihgfedcba") == (('zyxwvutsrqponmlkjihgfedcb', '+'), ('a', ' '), ('bcdefghijklmnopqrstuvwxyz', '-'))
    assert linediff2("abcdefghijklmnopqrstuvwxyz", "zyxwvutsrqponmlkjihgfedcba") == (('abcdefghijklmnopqrstuvwxy', '-'), ('z', ' '), ('yxwvutsrqponmlkjihgfedcba', '+'))

    assert linediff ("asdf", "jkle") == (('jkle', '!asdf'),)
    assert linediff2("asdf", "jkle") == (('jkle', '!asdf'),)

    assert linediff ("abcdefghijklmnopqrstuvwxyz", "lmnop") == (('abcdefghijk', '-'), ('lmnop', ' '), ('qrstuvwxyz', '-'))
    assert linediff2("abcdefghijklmnopqrstuvwxyz", "lmnop") == (('abcdefghijk', '-'), ('lmnop', ' '), ('qrstuvwxyz', '-'))

    assert linediff ("lmnop", "abcdefghijklmnopqrstuvwxyz") == (('abcdefghijk', '+'), ('lmnop', ' '), ('qrstuvwxyz', '+'))
    assert linediff2("lmnop", "abcdefghijklmnopqrstuvwxyz") == (('abcdefghijk', '+'), ('lmnop', ' '), ('qrstuvwxyz', '+'))

    print("All OK")


if __name__ == "__main__":
    main()

